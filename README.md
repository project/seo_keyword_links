
INTRODUCTION
------------

 Shows links for a given group of words in node body.

 Let's imagine you have a list of links you want they were displayed in
  your blog every time appears a given set of words in the body of your
   nodes.
 With this module you can set up a list of links for a group of words.
 The links can be whatever you want and you can change afterwards them.
   at the configuration page.

REQUIREMENTS
------------

 This module does not require any special module.

INSTALLATION
------------

  1. Download the module.
  2. Install/enable the module.

CONFIGURATION
-------------
 Go to admin/config/seo_keyword_links/seosettings and
 configure your words to be substituted by links


MAINTAINERS
-----------

 Current maintainer:

  * leandro713 (https://www.drupal.org/user/1071832)
