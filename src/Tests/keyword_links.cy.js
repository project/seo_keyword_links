
describe('Logs as admin', () => {
  it('Gains authentication with using correct login credentials and configure keywords module', () => {

    cy.visit('/user/logout')
    cy.visit('/user/login')
    cy.get('#edit-name')
      .type('admin')
    cy.get('#edit-pass')
      .type('CHANGE-ME')
    cy.get('input#edit-submit')
      .contains('Log in')
      .click()
      cy.get('.title').contains('admin');
    //clear cache
    cy.visit('/admin/config/development/performance')
    cy.get('#edit-clear').click()

    
    cy.visit('/admin/config/seo_keyword_links/seosettings')
    // keywords
    cy.get('textarea#edit-links').type(
      `Ono -- //ono.es
      Vodafone -- //vodafone.es
      Movistar -- //movistar.es
      Yoigo -- //yoigo.es`);
    // content type
    cy.get('#edit-node-types-article').check()
    cy.get('input#edit-submit')
    cy.visit('/node/add/article')
    // add test node with keywords
    cy.get('input#edit-title-0-value').type('keywords test node!')
    
    cy.window().then(win => {
      win.Drupal.CKEditor5Instances.forEach(editor => editor.setData(`
      <p>T-bone short ribs jerky bresaola burgdoggen chislic Ono. Picanha t-bone shoulder, ribeye porchetta burgdoggen beef ribs chicken. Shank brisket ribeye, strip steak rump shankle
         pancetta kielbasa Vodafone meatball tri-tip sirloin drumstick tenderloin.</p>
         <p>Ham hock venison Movistar shoulder, tri-tip chuck hamburger Yoigo beef rump biltong shankle sausage.</p>`))
    })
    
    

    let x = Math.floor(Math.random() * 50) + 1;
    cy.get('#edit-path-0').click()
    cy.get('#edit-path-0-alias').type('/keyword-test-' + x);
    cy.get('input#edit-submit').contains('Save').click()

    cy.visit('/keyword-test-'+ x)
    //cy.get('a').contains('Ono').should('exist');
    //cy.get('a').contains('Vodafone').should('exist');
    cy.screenshot('keywords node example-' + x)

    


    cy.get('.tabs__link').contains("Delete").click()
    cy.get('input#edit-submit').contains('Delete').click()
    });
  });

