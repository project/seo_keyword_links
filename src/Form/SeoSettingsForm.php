<?php

namespace Drupal\seo_keyword_links\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Config form class for seo_keyword_links module.
 *
 * @package Drupal\seo_keyword_links\Form
 */
class SeoSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'seo_keyword_links.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'seo_settings_form';
  }

  /**
   * The entity type manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('seo_keyword_links.settings');

    $get_config_links = function () use ($config): string {
      return $config->get('links') ? $this->walkLinks($config->get('links')) : "";
    };

    $form['links'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Links'),
      '#default_value' => $get_config_links(),
      '#description' => $this->t('Please, enter each line with the following format
          "Word -- http://mylink.com"'),
    ];

    $contentTypes = $this->entityTypeManager->getStorage('node_type')->loadMultiple();

    $contentTypesList = [];
    foreach ($contentTypes as $contentType) {
      $contentTypesList[$contentType->id()] = $contentType->label();
    }

    $form['node_types'] = [
      '#type' => 'checkboxes',
      '#options' => $contentTypesList,
      '#title' => $this->t('Node types'),
      '#default_value' => $this->config('seo_keyword_links.settings')->get("node_types"),
      '#description' => $this->t('Select the content type to apply the desired behaviour. Clear the cache after changing this.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $content_types = [];
    foreach ($form_state->getValue('node_types') as $content_type) {
      if ($content_type !== "0") {
        $content_types[] = $content_type;
      }
    }

    $this->config('seo_keyword_links.settings')
      ->set('links', $this->codeLinks(explode("\n", $form_state->getValue('links'))))
      ->set('node_types', $content_types)
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Returns string of array key-value pairs concatenated by '--'.
   *
   * @param array $arr
   *   The array to iterate over.
   *
   * @return string
   *   A string containing the concatenated elements of the array.
   */
  private function walkLinks(array $arr): string {
    // Initialize variable for concatenating values.
    $result = '';

    // Iterate over the array and concatenate keys and values.
    foreach ($arr as $key => $value) {
      $result .= $key . ' -- ' . $value . "\n";
    }

    // Return the resulting string.
    return $result;
  }

  /**
   * Parses array of strings into associative array of links.
   *
   * @param array $arr
   *   An array of strings containing links in the following format:
   *   "
   *     link_title1 -- link_url1
   *     link_title2 -- link_url2
   *   ".
   *
   * @return array
   *   An associative array where the keys are the link titles and the values
   *   are the link URLs.
   */
  private function codeLinks(array $arr): array {
    // Initialize the result array.
    $result = [];

    // Iterate over the array and extract the link titles and URLs.
    foreach ($arr as $value) {
      $parts = explode(' -- ', $value);
      if (isset($parts[0]) && isset($parts[1])) {
        $result[$parts[0]] = $parts[1];
      }
    }

    // Return the resulting array.
    return $result;
  }

}
